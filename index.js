var utils = require('./services/excelUtils.js');
var computations = require('./services/excelComputations.js');
var matrix = require('./services/excelMatrix.js');

module.exports.calc = require("./services/matrixCalc.js");
if (typeof window !== "undefined"){
    window.Matrix = matrix;
}

