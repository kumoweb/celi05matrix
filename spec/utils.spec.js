'use strict';
var excelUtils = require('../services/excelUtils.js');
describe('Services: Excel utilities', function () {
  // Initialize the controller and a mock scope
  function round(nb){
      return Math.round(nb *100) / 100;
  }
  it('PV Function should return the same values as excel', function () {
    expect(round(excelUtils.PV(0.0025,288, 946.49))).toBe(-194147.89);
    expect(round(excelUtils.PV(0.0025,250, 946.49))).toBe(-175790.05);
    expect(round(excelUtils.PV(0.0025,288, 1200.35))).toBe(-246220.69);
    expect(round(excelUtils.PV(0.00175,288, 946.49))).toBe(-213974.05);
  });

  it("PMT function should return the same values as excel", function(){
    expect(round(excelUtils.PMT(0.05, 245, -150000,0,0))).toBe(7500.05);
    expect(round(excelUtils.PMT(0.05, 275, -200000,0,0))).toBe(10000.01);
    expect(round(excelUtils.PMT(0.003455, 245, -200000,0,0))).toBe(1211.33);
    expect(round(excelUtils.PMT(0.05, 245, -200000,0,0))).toBe(10000.06);
  });

  it("Adjusted rate function should return the same value as excel would", function(){
    expect(excelUtils.adjustedRate(0.05)).toBe(0.0041239154651442345);
  });


  it("Adjusted rendement function should return the same value as excel would", function(){
    expect(excelUtils.adjustedRendement(0.05, 12)).toBe(1.0511618978817334);
  });
});
