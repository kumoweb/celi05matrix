'use strict';
var excelComputations = require("../services/excelComputations.js");
describe('Services: Excel utilities', function () {

  // load the service's module

  function round(nb){
      return Math.round(nb *100) / 100;
  }
  // Initialize the controller and a mock scope

  it("Cout placement should return the same value as in the excel", function(){
    expect(round(excelComputations.coutPlacement(0.03, 300, 0))).toBe(0);
    expect(round(excelComputations.coutPlacement(0.03, 300, 2000))).toBe(9.46);
  });

  it("Paiement mensuelle should return the same value as in the excel", function(){
    expect(round(excelComputations.paiementMensuel(0.03, 202000, 300, true))).toBe(955.96);
    expect(round(excelComputations.paiementMensuel(0.03, 200000, 300, true))).toBe(946.49);
    expect(round(excelComputations.paiementMensuel(0.03, 200000, 300, false))).toBe(500);
  });

  it("Economie mensuelle should return the same value as in the excel", function(){
    expect(excelComputations.economieMensuelle(946.49, 1446.49, 0)).toBe(500);
    expect(excelComputations.economieMensuelle(946.49, 1446.49, 20)).toBe(480);
  });

  it("Attribution mensuelle should return the same value as in the excel", function(){
    expect(excelComputations.attributionMensuelle(480.00, 0.75)).toBe(360);
  });

  it("celipotheque should return the same value as in the excel", function(){
    expect(round(excelComputations.celiPotheque(0.03,288,963.054589521511))).toBe(197545.69);
  });

  it("Emprunt deductible should return the same value as in the excel", function(){
    expect(round(excelComputations.empruntDeductible(0.03,288,9.46490997072738))).toBe(1941.48);
  });

  it("Valeur placement should return the same value as in the excel", function(){
    expect(round(excelComputations.valeurDePlacement(2000,0.05,12))).toBe(2102.32);
  });

  it("Epargne REER should return the same value as in the excel", function(){
    expect(round(excelComputations.epargneREER(463.435410478489,0.2,1500, 2))).toBe(1112.24);
    expect(round(excelComputations.epargneREER(463.435410478489,0.2,1500, 1))).toBe(2612.24);
  });

  it("Epargne CELI should return the same value as in the excel", function(){
    expect(round(excelComputations.epargneCELI(463.435410478489,0.7))).toBe(3892.86);
  });

  it("Valeur REER should return the same value as in the excel", function(){
    expect(round(excelComputations.valeurREER(463.435410478489,0.2,0.05,12,1500))).toBe(2714.83);
    expect(round(excelComputations.valeurREER(564.766775921236,0.3,0.05,12,2714.834133815))).toBe(4934.14);
  });

  it("Economie impot should return the same value as in the excel", function(){
    expect(round(excelComputations.economieImpot(1941.48094712672,0.03,2612.244985148,0.384))).toBe(1025.47);
  });

  it("Epargne non enregistre should return the same value as in the excel", function(){
    expect(round(excelComputations.epargneNonEnregistre(463.435410478489, 0.1))).toBe(556.12);
  });

  it("Valeur epargne should return the same value as in the excel", function(){
    expect(round(excelComputations.valeurEpargne(0.05, 463.435410478489,0.7,0.1, 0,0))).toBe(4552.37);
    expect(round(excelComputations.valeurEpargne(0.05, 463.435410478489,0.7,0.1, 4552.36514797, 1025.467934808))).toBe(10415.57);
  });

  it("Valeur nette liquide should return the same value as in the excel", function(){
    expect(round(excelComputations.valeurNetteLiquide(4552.365147970, 2102.323795763,197545.686370144))).toBe(-190891);
  });

  it("Impot Latent should return the same value as in the excel", function(){
    expect(round(excelComputations.impotLatent(2102.323795763, 2000, 0.384))).toBe(19.65);
  });

  it("Valeur nette totale should return the same value as in the excel", function(){
    expect(round(excelComputations.valeurNetteTotale(4552.365147970, 2102.323795763, 2714.834133815,197545.686370144, 19.65))).toBe(-188195.81);
  });
});
