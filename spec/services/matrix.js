/**
 * Created by ddugue on 3/12/15.
 */
'use strict';

describe('Services: Excel utilities', function () {

    // load the service's module
    beforeEach(module('app'));

    var matrix, $rootScope;

    function round(nb) {
        return Math.round(nb * 100) / 100;
    }

    function roundList(arr){
        var retour = [];
        var i;
        for (i=0;i<arr.length;i++){
            retour[i] = round(arr[i]);
        }
        return retour;
    }

    // Initialize the controller and a mock scope
    beforeEach(inject(function (_matrix_,_$rootScope_) {
        matrix = _matrix_;
        $rootScope = _$rootScope_;
    }));
    var d = {
        coutPlacement: 9.46490997072738,
        mensualiteActuelle:1446.49,
        coutAssurance:20,
        tauxImposition: 0.384,
        montantEmprunt: 203500,
        nbYears: 25,
        placementForfaitaire:2000,
        cotisationREER:1500,
        tauxRendement:0.05,
        paiementMensuel: [963.054589521511,
            861.723224078764,
            1070.451117768730,
            963.054589521511,
            1183.566144550330],
        economieMensuelle: [463.435410478489,
            564.766775921236,
            356.038882231272,
            463.435410478489,
            242.923855449673],
        attributionCeli: [
            0.7,
            0.6,
            0.9,
            0.6,
            0.45
        ],
        attributionREER: [
            0.2,
            0.3,
            0.1,
            0.25,
            0.35
        ],
        attributionNE: [
            0.1,
            0.1,
            0,
            0.15,
            0.2
        ],
        tauxInteret: [
            0.03,
            0.02,
            0.04,
            0.03,
            0.05
        ],
        celiPotheque: [
            197545.686370144,
            190513.864265342,
            187738.814127394,
            179894.481623799,
            179340.230597157],
        empruntDeductible: [
            1941.480947127,
            1885.379230419,
            1827.571126313,
            1768.004733403,
            1706.626571654],
        valeurPlacement: [
            2102.323795763,
            2209.882671117,
            2322.944462667,
            2441.790710051,
            2566.717357007],
        epargneREER: [
            2612.244985148,
            2033.160393316,
            427.246658678,
            1390.306231435,
            1020.280192889],
        valeurREER: [
            2714.834133815,
            4934.137088936,
            3896.449153126,
            7643.121447637,
            7822.031492310],
        economieImpot: [
            1025.467934808,
            802.453159768,
            185.116336307,
            554.245007400,
            411.447932175],
        epargneCELI: [
            3892.857448019,
            4066.320786633,
            3845.219928098,
            3336.734955445,
            1311.788819428],
        epargneNE: [
            556.122492574,
            677.720131105,
            0,
            834.183738861,
            583.017253079],
        valeurEpargne: [
            4552.365147970,
            10717.488348963,
            16043.898565687,
            21327.164431353,
            24939.741369713],
        valeurNetteLiquide: [
            -190890.997426410,
            -177586.493245263,
            -169371.971099041,
            -156125.526482395,
            -151833.771870436
        ],
        impotLatent: [19.6461687865855,
            40.2974728543976,
            62.0053368320516,
            84.8238163297610,
            108.8097325453490
        ],
        valeurNetteTotale: [-188195.809461381,
            -172692.653629181,
            -165537.527282747,
            -148567.228851088,
            -144120.550110672]
    };
    it("The matrix should run and return non empty value", function () {
        var matrixInstance = new matrix();
        var promise = matrixInstance.getOutput({
            tauxInteret: d.tauxInteret,
            attributionREER: d.attributionREER,
            attributionCELI: d.attributionCeli,
            attributionNE: d.attributionNE,
            mensualiteActuelle: d.mensualiteActuelle,
            montantEmprunt: d.montantEmprunt,
            nbYears: 25,
            placementForfaitaire: d.placementForfaitaire,
            tauxImposition: d.tauxImposition,
            tauxRendement: d.tauxRendement,
            cotisationREER: d.cotisationREER,
            coutMensuelAssurance: d.coutAssurance
        });
        expect(promise).not.toEqual(null);
        promise.then(function(output){
            // console.log(output);
            expect(matrixInstance.output).toBe(output);
            expect(output.celipotheque).not.toEqual([]);
            expect(output.empruntDeductible).not.toEqual([]);
            expect(output.coutPlacement).not.toEqual([]);
            expect(output.valeurPlacement).not.toEqual([]);
            expect(output.epargneREER).not.toEqual([]);
            expect(output.valeurREER).not.toEqual([]);
            expect(output.economieImpot).not.toEqual([]);
            expect(output.epargneCELI).not.toEqual([]);
            expect(output.epargneNE).not.toEqual([]);
            expect(output.valeurEpargne).not.toEqual([]);
            expect(output.valeurNetteLiquide).not.toEqual([]);
            expect(output.impotLatent).not.toEqual([]);
            expect(output.valeurNetteTotale).not.toEqual([]);
            expect(output.paiementMensuel).not.toEqual([]);
            expect(output.economieMensuelle).not.toEqual([]);
        });
        $rootScope.$digest();
    });
});
