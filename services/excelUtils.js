/**
 * Created by ddugue on 3/11/15. Contains mathematical abstract functions
 */
/* UTILITY FUNCTIONS */
function conv_number(expr, decplaces) { // This function is from David Goodman's Javascript Bible.
    var str = "" + Math.round(eval(expr) * Math.pow(10, decplaces));
    while (str.length <= decplaces) {
        str = "0" + str;
    }
    var decpoint = str.length - decplaces;
    return (str.substring(0, decpoint) + "." + str.substring(decpoint, str.length));
}

function PV(rate, nper, pmt) {
    return -(pmt / rate * (1 - Math.pow(1 + rate, -nper)));
}

function PMT(rate, nper, pv, fv, type) {
    if (!fv) fv = 0;
    if (!type) type = 0;
    if (rate == 0) return -(pv + fv) / nper;

    var pvif = Math.pow(1 + rate, nper);
    var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

    if (type == 1) {
        pmt /= (1 + rate);
    }
    ;
    return pmt;
}

function smallTest(value1, value2){
    return value1 + value2;
}
function adjustedRate(interestRate) {
    return Math.pow(1 + (interestRate / 2), 1 / 6) - 1;
}

function adjustedRendement(tauxRendement, nbmoisPasse) {
    return Math.pow(1 + (tauxRendement / 12), nbmoisPasse)
}

module.exports = {
    PV: PV,
    PMT: PMT,
    adjustedRate: adjustedRate,
    adjustedRendement: adjustedRendement,
    smallTest: smallTest
};
