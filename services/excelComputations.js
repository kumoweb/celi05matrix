/**
 * Created by ddugue on 3/11/15. Contains calculation that outputs single values
 */
var excelUtils = require('./excelUtils.js');
function coutPlacement(tauxInteret, nbMois, placementForfaitaire) {
    return excelUtils.PMT(excelUtils.adjustedRate(tauxInteret), nbMois, -placementForfaitaire, 0, 0);
}

function economieMensuelle(paiementMensuel, mensualiteActuelle, coutAssurance) {
    return mensualiteActuelle - paiementMensuel - coutAssurance
}

function paiementMensuel(tauxInteret, montantEmprunt, nbMois, capitalActive) {
    if (capitalActive){
        return excelUtils.PMT(excelUtils.adjustedRate(tauxInteret),nbMois, -montantEmprunt)
    }else{
        return montantEmprunt*tauxInteret/12
    }

}

function attributionMensuelle(economieMensuelle, pourcentage) {
    return economieMensuelle * pourcentage
}

function celiPotheque(tauxInteret, nbMoisRestant, paiementMensuel) {
    return -excelUtils.PV(excelUtils.adjustedRate(tauxInteret), nbMoisRestant, paiementMensuel);
}

function empruntDeductible(tauxInteret, nbMoisRestant, coutPlacement) {
    return -excelUtils.PV(tauxInteret / 12, nbMoisRestant, coutPlacement);
}

function valeurDePlacement(placementForfaitaire, tauxRendement, nbMoisPasse) {
    return placementForfaitaire * excelUtils.adjustedRendement(tauxRendement, nbMoisPasse);
}

function epargneREER(economieMensuelle, pourcentageREER, cotisationREER, anneeCourante) {
    if (anneeCourante == 1) {
        return (attributionMensuelle(economieMensuelle, pourcentageREER) * 12) + cotisationREER;
    } else {
        return (attributionMensuelle(economieMensuelle, pourcentageREER) * 12);
    }
}

function epargneCELI(economieMensuelle, pourcentageCELI) {
    return (attributionMensuelle(economieMensuelle, pourcentageCELI) * 12)
}

function valeurREER(economieMensuelle, pourcentageREER, tauxRendement, nbMoisPasse, valeurREER) {
    return (attributionMensuelle(economieMensuelle, pourcentageREER) * ((excelUtils.adjustedRendement(tauxRendement, nbMoisPasse) - 1) / (tauxRendement / 12))) + (valeurREER * excelUtils.adjustedRendement(tauxRendement, nbMoisPasse))
}

function economieImpot(empruntDeductible, tauxInteretFirst, epargneREER, tauxImpot) {
    return ((empruntDeductible * tauxInteretFirst) + epargneREER) * tauxImpot
}

function epargneNonEnregistre(economieMensuelle, pourcentageAutre) {
    return (attributionMensuelle(economieMensuelle, pourcentageAutre) * 12)
}

function valeurEpargne(tauxRendement, economieMensuelle, pourcentageCELI, pourcentageAutre, valeurEpargnePrecedente, valeurImpotPrecedente) {
    if (!valeurImpotPrecedente) valeurImpotPrecedente = 0;
    if (!valeurEpargnePrecedente) valeurEpargnePrecedente = 0;
    if (!pourcentageAutre) pourcentageAutre = 0;
    if (!pourcentageCELI) pourcentageCELI = 0;
    return ((valeurEpargnePrecedente + valeurImpotPrecedente) * excelUtils.adjustedRendement(tauxRendement, 12))
        + ((attributionMensuelle(economieMensuelle, pourcentageCELI) + attributionMensuelle(economieMensuelle, pourcentageAutre))
           * ((excelUtils.adjustedRendement(tauxRendement, 12) - 1) / (tauxRendement / 12)));
}

function valeurNetteLiquide(valeurEpargne, valeurPlacement, celipotheque) {
    return valeurEpargne + valeurPlacement - celipotheque;
}

function impotLatent(valeurPlacement, placementForfaitaire, tauxImpot) {
    return (valeurPlacement - placementForfaitaire) * 0.5 * tauxImpot;
}

function valeurNetteTotale(valeurEpargne,valeurPlacement,valeurREER,celipotheque, impotLatent) {
    return valeurEpargne + valeurPlacement + valeurREER - celipotheque - impotLatent;
}

module.exports = {
    coutPlacement: coutPlacement, /*PMT((((1+([TAUXINTERET]/2))^(1/6)) - 1), [NBMOIS],-[PLACEMENTFORFAITAIRE],0,0)*/
    economieMensuelle: economieMensuelle, /* Montant economise par mois */
    paiementMensuel: paiementMensuel, /*PMT((((1+([TAUX DINTERET]/2))^(1/6))-1), -[MONTANTEMPRUNT],[NB_MOIS], 0, 0)*/
    attributionMensuelle: attributionMensuelle,
    celiPotheque: celiPotheque, /* -PV([TAUXDINTERET]/12,[NBMOISPASSE],[PAIEMENTMENSUEL]) */
    empruntDeductible: empruntDeductible, /* -PV([TAUXDINTERET]/12,[NBMOISPASSE],[COUTPLACEMENT])*/
    valeurDePlacement: valeurDePlacement, /* ([PLACEMENTFORFAITAIRE]*(1+([TAUXRENDEMENT]/12))^([NBMOISPASSE])) */
    epargneREER: epargneREER,
    epargneCELI: epargneCELI,
    valeurREER: valeurREER, /*([ATTRMENSUELLEREER] * ((((1+([TAUXRENDEMENT]/12))^[NBMOISPASSE])-1)/([TAUXRENDEMENT]/12)))+([VALEURREEERANNEPASSE OU COTISATIONREER] * (1+([TAUXRENDEMENT]/12))^[NBMOISPASSE])*/
    economieImpot: economieImpot, /*(([EMPRUNTDEDUCTIBLE]* [TAUXINTERETFIRST])+([EPARGNEREER])) *[TAUXIMPOT]*/
    epargneNonEnregistre: epargneNonEnregistre,
    valeurEpargne: valeurEpargne,
    valeurNetteLiquide: valeurNetteLiquide,
    impotLatent: impotLatent,
    valeurNetteTotale: valeurNetteTotale
}
;
