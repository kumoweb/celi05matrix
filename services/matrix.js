/**
 * Created by ddugue on 3/11/15.
 */
angular.module("matrix").factory("matrix", function ($q, promiseWrapper, excelMatrix) {
    /* Usage:
        var matrix = new matrix();
        promise = matrix.getOutput({} params);
        promise.then(function(output){
            -- BIND IT TO YOUR SCOPE --
        });
     */
    var paramsDefault = {
        tauxInteret: [0.03],
        attributionREER: [0.0],
        attributionCELI: [1.0],
        attributionNE: [0.0],
        mensualiteActuelle: 2500.0,
        montantEmprunt: 2500.0,
        nbYears: 25,
        placementForfaitaire: 0.0,
        tauxImposition: 0.3875,
        tauxRendement: 0.05,
        cotisationREER: 0.0,
        coutMensuelAssurance: 0.0,
        capitalActive: true,
        soldeHypotheque: 0.0
    };

    var outputDefault = {
        celipotheque: [],
        empruntDeductible: [],
        coutPlacement: 0.0,
        valeurPlacement: [],
        epargneREER: [],
        valeurREER: [],
        economieImpot: [],
        epargneCELI: [],
        epargneNE: [],
        valeurEpargne: [],
        valeurNetteLiquide: [],
        impotLatent: [],
        valeurNetteTotale: [],
        paiementMensuel: [],
        economieMensuelle: [],
        capital:[]
    };

    return function () {
        var _this = this;
        this.output = outputDefault;
        this.input = paramsDefault;

        function accessor(attrName) {
            return function () {
                if (!(this[attrName])) return 0;
                return this[attrName];
            }
        }
        this.setParams = function (params) {
            this.input = angular.extend(this.input, params);
        };

        this.setOutput = function (attrName, promise) {
            return promise.then(function (data) {
                _this.output[attrName] = data;
                return data;
            })
        };
        this.getTauxInteret = new promiseWrapper(this.input, accessor("tauxInteret"));
        this.getAttributionREER = new promiseWrapper(this.input, accessor("attributionREER"));
        this.getAttributionCELI = new promiseWrapper(this.input, accessor("attributionCELI"));
        this.getAttributionNE = new promiseWrapper(this.input, accessor("attributionNE"));
        this.getMensualiteActuelle = new promiseWrapper(this.input, accessor("mensualiteActuelle"));
        this.getMontantEmprunt = new promiseWrapper(this.input, accessor("montantEmprunt"));
        this.getNbYears = new promiseWrapper(this.input, accessor("nbYears"));
        this.getPlacementForfaitaire = new promiseWrapper(this.input, accessor("placementForfaitaire"));
        this.getTauxImposition = new promiseWrapper(this.input, accessor("tauxImposition"));
        this.getTauxRendement = new promiseWrapper(this.input, accessor("tauxRendement"));
        this.getCotisationREER = new promiseWrapper(this.input, accessor("cotisationREER"));
        this.getCoutMensuelAssurance = new promiseWrapper(this.input, accessor("coutMensuelAssurance"));
        this.getCapitalActive = new promiseWrapper(this.input, accessor("capitalActive"));
        this.getSoldeHypotheque = new promiseWrapper(this.input, accessor("soldeHypotheque"));
        this.operations = new promiseWrapper(this, excelMatrix);

        this.getOutput = function (params) {
            function s(name, attr) {
                /* SHORTCUT FUNCTION */
                return _this.setOutput(
                    name,
                    _this.operations[name].eval(attr)
                )
            }

            this.setParams(params);

            var coutPlacement = s("coutPlacement", [
                this.getNbYears.wrap(),
                this.getTauxInteret.wrap(),
                this.getPlacementForfaitaire.wrap()
            ]);

            var paiementMensuel = s("paiementMensuel",[
                this.getNbYears.wrap(),
                this.getTauxInteret.wrap(),
                this.getMontantEmprunt.wrap(),
                this.getCapitalActive.wrap()
            ]);

            var economieMensuelle = s("economieMensuelle",[
                this.getNbYears.wrap(),
                this.getMensualiteActuelle.wrap(),
                this.getCoutMensuelAssurance.wrap(),
                paiementMensuel
            ]);

            var celipotheque = s("celipotheque",[
                this.getNbYears.wrap(),
                this.getTauxInteret.wrap(),
                paiementMensuel,
                this.getCapitalActive.wrap(),
                this.getSoldeHypotheque.wrap(),
                this.getMontantEmprunt.wrap()
            ]);

            var empruntDeductible = s("empruntDeductible",[
                this.getNbYears.wrap(),
                this.getTauxInteret.wrap(),
                coutPlacement,
                this.getCapitalActive.wrap(),
                this.getPlacementForfaitaire.wrap()
            ]);

            var valeurPlacement = s("valeurPlacement",[
                this.getNbYears.wrap(),
                this.getPlacementForfaitaire.wrap(),
                this.getTauxRendement.wrap()
            ]);

            var epargneREER = s("epargneREER",[
                this.getNbYears.wrap(),
                this.getAttributionREER.wrap(),
                this.getCotisationREER.wrap(),
                economieMensuelle
            ]);

            var epargneCELI = s("epargneCELI",[
                this.getNbYears.wrap(),
                this.getAttributionCELI.wrap(),
                economieMensuelle
            ]);

            var valeurREER = s("valeurREER",[
                this.getNbYears.wrap(),
                this.getAttributionREER.wrap(),
                this.getTauxRendement.wrap(),
                this.getCotisationREER.wrap(),
                economieMensuelle
            ]);

            var economieImpot = s("economieImpot",[
                this.getNbYears.wrap(),
                this.getTauxInteret.wrap(),
                this.getTauxImposition.wrap(),
                epargneREER,
                empruntDeductible
            ]);

            var epargneNonEnregistre = s("epargneNE",[
                this.getNbYears.wrap(),
                this.getAttributionNE.wrap(),
                economieMensuelle
            ]);

            var valeurEpargne = s("valeurEpargne",[
                this.getNbYears.wrap(),
                this.getTauxRendement.wrap(),
                this.getAttributionCELI.wrap(),
                this.getAttributionNE.wrap(),
                economieMensuelle,
                economieImpot
            ]);

            var valeurNetteLiquide = s("valeurNetteLiquide",[
                this.getNbYears.wrap(),
                valeurEpargne,
                valeurPlacement,
                celipotheque
            ]);

            var impotLatent = s("impotLatent",[
                this.getNbYears.wrap(),
                this.getPlacementForfaitaire.wrap(),
                this.getTauxImposition.wrap(),
                valeurPlacement
            ]);

            var valeurNetteTotale = s("valeurNetteTotale",[
                this.getNbYears.wrap(),
                valeurEpargne,
                valeurPlacement,
                valeurREER,
                celipotheque,
                impotLatent
            ]);

            var capital = s("capital",[
                this.getNbYears.wrap(),
                valeurEpargne,
                valeurPlacement,
                valeurREER,
                impotLatent
            ]);
            return $q.all([
                    paiementMensuel,
                    economieMensuelle,
                    celipotheque,
                    coutPlacement,
                    valeurNetteTotale,
                    impotLatent,
                    valeurNetteLiquide,
                    valeurEpargne,
                    epargneNonEnregistre,
                    economieImpot,
                    valeurREER,
                    epargneCELI,
                    epargneREER,
                    valeurPlacement,
                    empruntDeductible,
                    capital
                ]).then(function () {
                    return _this.output
                });
        };

        return this;
    }
});
