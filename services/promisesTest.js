angular.module('matrix').factory("TestPromise", function ($q, promiseWrapper, excelMatrix) {
        return function (x, y) {
            var _this = this;
            this.x = x;
            this.y = y;
            this.getX = function(){
                console.log(this);
                return this.x
            };
            this.getY = function(){
                console.log(this);
                return this.y
            };

            var getX = new promiseWrapper(this,this.getX);
            var getY = new promiseWrapper(this,this.getY);
            var operations = new promiseWrapper(this, excelMatrix);
            this.getValue = function () {
                var XResult = operations.operationX.eval([getX.wrap()]);
                var YResult = operations.operationY.eval([getY.wrap()]);
                var ZResult = operations.operationZ.eval([XResult,YResult]);
                return ZResult
            }

        }

    });
