/**
 * Created by ddugue on 3/9/15.
 */
angular.module("matrix").factory("promiseWrapper", function($q){
    var wrapper =  function (ref,fn) {
        if (fn && fn !== null && typeof fn === 'object'){
            var retour = {};
            angular.forEach(fn, function(value, key) {
                if(value && typeof value === 'function' && !(key.indexOf("_") == 0)){
                    retour[key] = new wrapper(ref, value);
                }else{
                    retour[key] = value;
                }
            });
            return retour;
        }else{
            /** It's a function **/
            if (typeof fn == 'function'){
                this.cacheArgs = [];
                this.cacheResult = null;
                var promiseThis = this;
                var _this = ref;
                return {
                    wrap: function (args) {
                        return $q(function (resolve, reject) {
                            if (args && angular.equals(args,promiseThis.cacheArgs)) {
                                console.log("Cached args")
                            } else {
                                if (!args){
                                    args = [];
                                }
                                promiseThis.cacheArgs = args;
                                promiseThis.cacheResult = fn.apply(_this,args);
                            }
                            resolve(promiseThis.cacheResult);
                        })
                    },
                    eval: function(promises){
                        return $q.all(promises).then(this.wrap)
                    }
                }
            }else{
                return null
            }
        }
    };
    return wrapper;
});
