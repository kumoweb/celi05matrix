var m = require('./excelMatrix.js');
module.exports = function(params, cb){

    var input = {
        tauxInteret: [0.03],
        attributionREER: [0.0],
        attributionCELI: [1.0],
        attributionNE: [0.0],
        mensualiteActuelle: 2500.0,
        montantEmprunt: 2500.0,
        nbYears: 25,
        placementForfaitaire: 0.0,
        tauxImposition: 0.3875,
        tauxRendement: 0.05,
        cotisationREER: 0.0,
        coutMensuelAssurance: 0.0,
        capitalActive: true,
        soldeHypotheque: 0.0
    };

    var output = {
        celipotheque: [],
        empruntDeductible: [],
        coutPlacement: 0.0,
        valeurPlacement: [],
        epargneREER: [],
        valeurREER: [],
        economieImpot: [],
        epargneCELI: [],
        epargneNE: [],
        valeurEpargne: [],
        valeurNetteLiquide: [],
        impotLatent: [],
        valeurNetteTotale: [],
        paiementMensuel: [],
        economieMensuelle: [],
        capital:[]
    };

    for (var attrname in params) { input[attrname] = params[attrname]; }
    output.coutPlacement = m.coutPlacement(input.nbYears, input.tauxInteret, input.placementForfaitaire);

    output.paiementMensuel = m.paiementMensuel(input.nbYears, input.tauxInteret, input.montantEmprunt, input.capitalActive);

    output.economieMensuelle = m.economieMensuelle(input.nbYears, input.mensualiteActuelle,input.coutMensuelAssurance, output.paiementMensuel);

    output.celipotheque = m.celipotheque(input.nbYears, input.tauxInteret, output.paiementMensuel, input.capitalActive, input.soldeHypotheque, input.montantEmprunt);

    output.empruntDeductible = m.empruntDeductible(input.nbYears, input.tauxInteret, output.coutPlacement, input.capitalActive, input.placementForfaitaire);

    output.valeurPlacement = m.valeurPlacement(input.nbYears, input.placementForfaitaire, input.tauxRendement);

    output.epargneREER = m.epargneREER(input.nbYears, input.attributionREER, input.cotisationREER, output.economieMensuelle);

    output.epargneCELI = m.epargneCELI(input.nbYears,
                                       input.attributionCELI,
                                       output.economieMensuelle);

    output.valeurREER = m.valeurREER(input.nbYears,
                                     input.attributionREER,
                                     input.tauxRendement,
                                     input.cotisationREER,
                                     output.economieMensuelle);

    output.economieImpot = m.economieImpot(input.nbYears,
                                           input.tauxInteret,
                                           input.tauxImposition,
                                           output.epargneREER,
                                           output.empruntDeductible);

    output.epargneNE = m.epargneNE(input.nbYears,
                                   input.attributionNE,
                                   output.economieMensuelle);

    output.valeurEpargne = m.valeurEpargne(input.nbYears,
                                           input.tauxRendement,
                                           input.attributionCELI,
                                           input.attributionNE,
                                           output.economieMensuelle,
                                           output.economieImpot);

    output.valeurNetteLiquide = m.valeurNetteLiquide(input.nbYears,
                                                     output.valeurEpargne,
                                                     output.valeurPlacement,
                                                     output.celipotheque);

    output.impotLatent = m.impotLatent(input.nbYears,
                                       input.placementForfaitaire,
                                       input.tauxImposition,
                                       output.valeurPlacement);

    output.valeurNetteTotale = m.valeurNetteTotale(input.nbYears,
                                                   output.valeurEpargne,
                                                   output.valeurPlacement,
                                                   output.valeurREER,
                                                   output.celipotheque,
                                                   output.impotLatent);

    output.capital = m.capital(input.nbYears,
                               output.valeurEpargne,
                               output.valeurPlacement,
                               output.valeurREER,
                               output.impotLatent);

    return output;
}
