/**
 * Created by ddugue on 3/11/15. Contain excel functions to return columns of data
 */

var utils = require("./excelUtils.js");
var computations = require("./excelComputations.js");

function stretchList(list, nbYears) {
    /* Return an array with the last value repeated until the end of the array*/
    if (list.length == nbYears) {
        return list;
    }
    var i;
    for (i = 0; i < nbYears; i++) {
        if (list.length <= i) {
            list[i] = list[i - 1];
        }
    }
    return list;
}
var forYears;
function inject(fn){
    forYears = fn;
};
function forYearsFn(nbYears, fn) {
    var i;
    var retour = [];
    for (i = 0; i < nbYears; i++) {
        retour[i] = fn(i,retour);
    }
    return retour;
};
inject(forYearsFn);
function coutPlacement(nbYears, tauxInteretList, placementForfaitaire) {
    return computations.coutPlacement(tauxInteretList[0], nbYears * 12, placementForfaitaire)
}

function economieMensuelle(nbYears, mensualiteActuelle, coutAssurance, paiementMensuelList) {

    paiementMensuelList = stretchList(paiementMensuelList, nbYears);

    return forYears(nbYears, function (i) {
        return computations.economieMensuelle(paiementMensuelList[i], mensualiteActuelle, coutAssurance)
    });
}

function paiementMensuel(nbYears, tauxInteretList, montantEmprunt, capitalActive) {

    var nbMois = nbYears * 12;

    tauxInteretList = stretchList(tauxInteretList, nbYears);

    return forYears(nbYears, function (i) {
        return computations.paiementMensuel(tauxInteretList[i], montantEmprunt, nbMois, capitalActive)
    });
}

function attributionMensuelle(nbYears, economieMensuelleList, pourcentageList) {

    economieMensuelleList = stretchList(economieMensuelleList, nbYears);
    pourcentageList = stretchList(pourcentageList, nbYears);

    return forYears(nbYears, function (i) {
        return computations.attributionMensuelle(economieMensuelleList[i], pourcentageList[i]);
    });
}

function celipotheque(nbYears, tauxInteretList, paiementMensuelList, capitalActif, soldeHypotheque, montantEmprunt) {
    var nbMoisRestant;
    var nbMois = nbYears * 12;

    tauxInteretList = stretchList(tauxInteretList, nbYears);

    return forYears(nbYears, function (i) {
        nbMoisRestant = nbMois - (i + 1) * 12;
        if (!capitalActif) { return montantEmprunt; }
        return computations.celiPotheque(tauxInteretList[i], nbMoisRestant, paiementMensuelList[i]);
    });
}

function empruntDeductible(nbYears, tauxInteretList, coutPlacement, capitalActif, placementForfaitaire) {
    var nbMoisRestant;
    var nbMois = nbYears * 12;

    tauxInteretList = stretchList(tauxInteretList, nbYears);

    return forYears(nbYears, function (i) {
        nbMoisRestant = nbMois - ((i + 1) * 12);
        if (!capitalActif){ return placementForfaitaire }
        return computations.empruntDeductible(tauxInteretList[0], nbMoisRestant, coutPlacement);
    });
}

function valeurPlacement(nbYears, placementForfaitaire, tauxRendement) {
    var nbMoisPasse;

    return forYears(nbYears, function (i) {
        nbMoisPasse = (i + 1) * 12;
        return computations.valeurDePlacement(placementForfaitaire, tauxRendement, nbMoisPasse);
    });
}

function epargneREER(nbYears, pourcentageREERList, cotisationREER, economieMensuelleList) {

    economieMensuelleList = stretchList(economieMensuelleList, nbYears);
    pourcentageREERList = stretchList(pourcentageREERList, nbYears);

    return forYears(nbYears, function (i) {
        return computations.epargneREER(economieMensuelleList[i], pourcentageREERList[i], cotisationREER, i + 1);
    });
}

function epargneCELI(nbYears, pourcentageCELIList, economieMensuelleList) {

    economieMensuelleList = stretchList(economieMensuelleList, nbYears);
    pourcentageCELIList = stretchList(pourcentageCELIList, nbYears);

    return forYears(nbYears, function (i) {
        return computations.epargneCELI(economieMensuelleList[i], pourcentageCELIList[i]);
    });
}

function valeurREER(nbYears, pourcentageREERList, tauxRendement, cotisationREER, economieMensuelleList) {
    var nbMoisPasse;

    economieMensuelleList = stretchList(economieMensuelleList, nbYears);
    pourcentageREERList = stretchList(pourcentageREERList, nbYears);
    return forYears(nbYears, function (i,retour) {
        nbMoisPasse = i * 12;
        if (i == 0) {
            return computations.valeurREER(economieMensuelleList[i], pourcentageREERList[i], tauxRendement,12, cotisationREER);
        } else {
            return computations.valeurREER(economieMensuelleList[i], pourcentageREERList[i], tauxRendement,nbMoisPasse, retour[0]);
        }

    });
}

function economieImpot(nbYears, tauxInteretList, tauxImpot, epargneREERList, empruntDeductibleList) {

    return forYears(nbYears, function (i) {
        return computations.economieImpot(empruntDeductibleList[i], tauxInteretList[0], epargneREERList[i], tauxImpot);
    });
}

function epargneNonEnregistre(nbYears, pourcentageAutreList, economieMensuelleList) {

    economieMensuelleList = stretchList(economieMensuelleList, nbYears);
    pourcentageAutreList = stretchList(pourcentageAutreList, nbYears);

    return forYears(nbYears, function (i) {
        return computations.epargneNonEnregistre(economieMensuelleList[i], pourcentageAutreList[i]);
    });
}

function valeurEpargne(nbYears, tauxRendement, pourcentageCELIList, pourcentageAutreList,economieMensuelleList, economieImpotList) {

    economieMensuelleList = stretchList(economieMensuelleList, nbYears);
    pourcentageAutreList = stretchList(pourcentageAutreList, nbYears);
    pourcentageCELIList = stretchList(pourcentageCELIList, nbYears);

    return forYears(nbYears, function (i,retour) {
        if (i > 0) {
            return computations.valeurEpargne(tauxRendement, economieMensuelleList[i], pourcentageCELIList[i], pourcentageAutreList[i], retour[i - 1], economieImpotList[i - 1]);
        } else {
            return computations.valeurEpargne(tauxRendement, economieMensuelleList[i], pourcentageCELIList[i], pourcentageAutreList[i], 0, 0);
        }
    });
}

function valeurNetteLiquide(nbYears, valeurEpargneList, valeurPlacementList, celipothequeList) {

    return forYears(nbYears, function (i) {
        return computations.valeurNetteLiquide(valeurEpargneList[i], valeurPlacementList[i], celipothequeList[i]);
    });
}

function impotLatent(nbYears, placementForfaitaire, tauxImpot, valeurPlacementList) {

    return forYears(nbYears, function (i) {
        return computations.impotLatent(valeurPlacementList[i], placementForfaitaire, tauxImpot);
    });
}

function valeurNetteTotale(nbYears, valeurEpargneList, valeurPlacementList, valeurREERList, celipothequeList, impotLatentList) {

    return forYears(nbYears, function (i) {
        return computations.valeurNetteTotale(valeurEpargneList[i], valeurPlacementList[i], valeurREERList[i], celipothequeList[i], impotLatentList[i]);
    });
}

function capital(nbYears, valeurEpargneList, valeurPlacementList, valeurREERList, impotLatentList) {

    return forYears(nbYears, function (i) {
        return computations.valeurNetteTotale(valeurEpargneList[i], valeurPlacementList[i], valeurREERList[i], 0, impotLatentList[i]);
    });
}
module.exports = {
    inject: inject,
    economieMensuelle: economieMensuelle, /* Montant economise par mois */
    paiementMensuel: paiementMensuel, /*PMT((((1+([TAUX DINTERET]/2))^(1/6))-1), -[MONTANTEMPRUNT],[NB_MOIS], 0, 0)*/
    attributionMensuelle: attributionMensuelle,
    celipotheque: celipotheque, /* -PV([TAUXDINTERET]/12,[NBMOISPASSE],[PAIEMENTMENSUEL]) */
    empruntDeductible: empruntDeductible, /* -PV([TAUXDINTERET]/12,[NBMOISPASSE],[COUTPLACEMENT])*/
    valeurPlacement: valeurPlacement, /* ([PLACEMENTFORFAITAIRE]*(1+([TAUXRENDEMENT]/12))^([NBMOISPASSE])) */
    epargneREER: epargneREER,
    epargneCELI: epargneCELI,
    valeurREER: valeurREER, /*([ATTRMENSUELLEREER] * ((((1+([TAUXRENDEMENT]/12))^[NBMOISPASSE])-1)/([TAUXRENDEMENT]/12)))+([VALEURREEERANNEPASSE OU COTISATIONREER] * (1+([TAUXRENDEMENT]/12))^[NBMOISPASSE])*/
    economieImpot: economieImpot, /*(([EMPRUNTDEDUCTIBLE]* [TAUXINTERETFIRST])+([EPARGNEREER])) *[TAUXIMPOT]*/
    epargneNE: epargneNonEnregistre,
    valeurEpargne: valeurEpargne,
    valeurNetteLiquide: valeurNetteLiquide,
    impotLatent: impotLatent,
    valeurNetteTotale: valeurNetteTotale,
    coutPlacement: coutPlacement,
    capital: capital
}
