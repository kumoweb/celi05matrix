# This is a README file for the REST API for celipothèque

# Vagrant running
Please ensure you have the following ansible packages:

+ kumo-ansible-nodejs
+ kumo-ansible-mongodb
+ kumo-ansible-common
+ kumo-ansible-nginx
